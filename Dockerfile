FROM mysql:5.6

MAINTAINER Warren Roque <wroquem@gmail.com>

COPY 01.-Scripts.sql /docker-entrypoint-initdb.d